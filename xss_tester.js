(function ($, Drupal, undefined) {
  // The selector list that can be prefilled.
  // @todo Update with HTML5 input types.
  var selectors = 'input[type="text"],textarea';

  /**
   * .each() callback to populate a DOM element with an XSS string.
   *
   * @param unused
   * @param element
   */

  function addXSS(unused, element) {
    var $element = $(element);
    var $form = $element.parents('form.xss-tester-enabled');
    var form_id = $('input[name="form_id"]', $form).val();
    var name = $element.attr('name');
    var xss = '<script>alert("XSS: ' + form_id + ':' + name + '")</script>';

    $element.val(xss);
  }

  /**
   * Attach handlers for the XSS Tester module.
   */

  Drupal.behaviors.xss_tester = {
    attach: function (context, settings) {
      $('.xss-tester-fieldset', context).once('xss-tester').removeClass('element-hidden');

      $('.xss-tester-prefill-all', context).once('xss-tester').bind('click', function (e) {
        e.preventDefault();

        var $form = $(this).parents('form.xss-tester-enabled');

        $(selectors, $form).each(addXSS);
      });

      $('.xss-tester-prefill-empty', context).once('xss-tester').bind('click', function (e) {
        e.preventDefault();

        var $form = $(this).parents('form.xss-tester-enabled');

        $(selectors, $form).filter(function() { return this.value == ""; }).each(addXSS);

      });
    }
  }
})(jQuery, Drupal);
