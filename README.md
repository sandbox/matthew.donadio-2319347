Introduction
------------

The XSS Tester module allows module developers and site owners to test
for [XSS](http://en.wikipedia.org/wiki/Cross-site_scripting) vulnerabilities
in their projects.

When enabled and configured, it will add two buttons at the top of Drupal
forms.  These buttons can prefill text inputs with an XSS test.  If this form
value is used in an insecure manner, the alert will be displayed showing
the $form_id and element name.

*Do not use this module on a production site.*

*Do not use this module on a development site and assign permissions to
untrusted users.*

If you do either of these things, you may risk a security vulnerability being
found without your knowledge.

If you discover a security vulnerability with either Drupal core, or a
contributed module, please keep it confidential and [submit your concern to the
Drupal security team](https://www.drupal.org/node/101494).

*Do not rely on this module as the sole testing for handling use input.*

This module is a tool to help find XSS issues, but developers should also be
familiar with how to [handle text in a secure fashion](https://www.drupal.org/node/28984).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/matthew.donadio/2319347
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2319347

Requirements
------------

This module has no dependencies on any core or contributed modules.
Javascript must be enabled in the client browser.


Recommended modules
-------------------

 * [Markdown filter](https://www.drupal.org/project/markdown):
   When the enabled, display of the project's README.md help will be
   rendered with markdown.

Installation
------------

 * This project is only available as a sandbox module.  To install it, visit
   the project page to get the repository information.  Then you `git clone`
   this into your site.

Configuration
-------------

 * Configure user permissions in Administration » People » Permissions:
   - Assign the "Allow XSS prefill" permission to the roles that you want to
     be able to test out forms.
 * Customize the behavior in Administration » Configuration » Development »
   XSS Tester.
   - "no forms" (the default value) will effectively disable the module
   - "all forms: will add the prefill buttons to all Drupal forms
   - "only forms below" will reveal a textarea where you can specify form IDs,
     one per line, to add the functionality to.
   - "exclude forms below" will reveal a textarea where you can specify form
     IDs, one per line, to *not* add the functionality to.

FAQ
---

### Why did you write this module?

As a project review administrator, I find a lot of XSS in project
applications.  I wanted to share a way to help module developers test their
modules, and also have an easy way for me to test submissions.

Maintainers
-----------

Current maintainers:

 * [Matthew Donadio (mpdonadio)](https://www.drupal.org/u/mpdonadio)
