<?php

/**
 * @file
 * XSS Tester module.
 */

/**
 * Implements hook_help().
 *
 * This will display the README.md as the help text, processed with the
 * markdown module, if installed.
 */
function xss_tester_help($path, $arg) {
  switch ($path) {
    case 'admin/help#xss_tester':
      $path = dirname(__FILE__) . '/README.md';
      if (file_exists($path)) {
        $readme = file_get_contents($path);
      }

      if (!isset($readme)) {
        return NULL;
      }

      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $function = $info['process callback'];
          $output = filter_xss_admin($function($readme, NULL));
        }
        else {
          $output = '<pre>' . check_plain($readme) . '</pre>';
        }
      }
      else {
        $output = '<pre>' . check_plain($readme) . '</pre>';
      }
      return $output;
  }
}

/**
 * Implements hook_permission().
 */

function xss_tester_permission() {
  return array(
    'use xss prefill' => array(
      'title' => t('Allow XSS prefill'),
      'description' => t('Adds buttons to forms to prefill elements with XSS.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function xss_tester_menu() {
  $items = array();

  $items['admin/config/development/xss-tester'] = array(
    'title' => 'XSS Tester',
    'description' => 'Configure XSS Tester options.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xss_tester_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'xss_tester.admin.inc',
    'file_path' => drupal_get_path('module', 'xss_tester'),
  );

  return $items;
}

/**
 * Implments hook_form_alter().
 */

function xss_tester_form_alter(&$form, &$form_state, $form_id) {
  if (!user_access('use xss prefill')) {
    return;
  }
  else if (!_xss_tester_check_form_id($form_id)) {
    return;
  }

  // Standard Drupal themes don't have a html.no-js class, so we use
  // element-hidden to hide the fieldset, and reveal it in JS.  This
  // will work with more themes, rather than setting display ourselves.

  $form['xss_tester'] = array(
    '#type' => 'fieldset',
    '#title' => t('XSS Prefill'),
    '#weight' => -100,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array(
      'class' => array('xss-tester-fieldset', 'element-hidden'),
    ),
  );

  $form['xss_tester']['xss_tester_all'] = array(
    '#type' => 'button',
    '#value' => t('Prefill All'),
    '#attributes' => array(
      'class' => array('xss-tester-prefill', 'xss-tester-prefill-all'),
    ),
  );

  $form['xss_tester']['xss_tester_empty'] = array(
    '#type' => 'button',
    '#value' => t('Prefill Empty'),
    '#attributes' => array(
      'class' => array('xss-tester-prefill', 'xss-tester-prefill-empty'),
    ),
  );

  $form['#attributes']['class'][] = 'xss-tester-enabled';

  $form['#attached']['js'][] = drupal_get_path('module', 'xss_tester') . '/xss_tester.js';

  // We need to disable browser XSS detection to use this module.
  // @see https://www.owasp.org/index.php/List_of_useful_HTTP_headers
  $form['#attached']['drupal_add_http_header'][] = array('X-XSS-Protection', '0');
}

function _xss_tester_check_form_id($form_id) {
  $method = variable_get('xss_tester_method', 'none');

  if ($method == 'none') {
    return FALSE;
  }
  if ($method == 'all') {
    return TRUE;
  }
  else {
    $form_ids = array_filter(explode(PHP_EOL, variable_get('xss_tester_form_ids', '')));
    $found = in_array($form_id, $form_ids);

    switch ($method) {
      case 'include':
        return $found;
      case 'exclude':
        return !$found;
      default:
        return FALSE;
    }
  }
}
