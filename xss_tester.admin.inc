<?php

/**
 * @file
 * Admin page callbacks for the XSS Tester module.
 */

/**
 * Settings form for admin/config/development/xss-tester
 */
function xss_tester_settings_form($form, &$form_state) {
  $form['xss_tester_method'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#description' => t('Choose which forms should allow XSS prefilling.'),
    '#options' => array(
      'none' => t('no forms'),
      'all' => t('all forms'),
      'include' => t('only forms below'),
      'exclude' => t('exclude forms below'),
    ),
    '#default_value' => variable_get('xss_tester_method', 'none'),
    '#required' => TRUE,
  );

  $form['xss_tester_form_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs'),
    '#description' => t('The form IDs (the machine name), one per line.'),
    '#default_value' => variable_get('xss_tester_form_ids', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="xss_tester_method"]' => array(
          array('value' => t('include')),
          array('value' => t('exclude'))
        ),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Validation for xss_tester_settings_form().
 */
function xss_tester_settings_form_validate($form, &$form_state) {
  $method = $form_state['values']['xss_tester_method'];

  if ($method == 'include' || $method == 'exclude') {
    if (!empty($form_state['values']['xss_tester_form_ids'])) {
      $form_ids = array_filter(explode(PHP_EOL, $form_state['values']['xss_tester_form_ids']));

      foreach ($form_ids as $form_id) {
        if (!preg_match('/^[a-zA-Z0-9_]+$/', $form_id)) {
          form_set_error('xss_tester_form_ids', t('%form_id is a bad form_id', array('%form_id' => $form_id)));
        }
      }
    }
  }
}
